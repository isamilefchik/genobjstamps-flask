import cv2
from PIL import Image, ImageTk
import io
import tensorflow as tf
from flask_app.bbx import replacement_inference_cs_kas_draw_bbx
import numpy as np
import random
import scipy.interpolate as inter

tf.enable_eager_execution()

z_to_l_model = "flask_app/static/giraffe/frozen_model_z_to_l.pb"
l_to_m_model = "flask_app/static/giraffe/frozen_model_l_to_m.pb"
m_to_t_model = "flask_app/static/giraffe/frozen_model_m_to_t.pb"

orig_im = 'flask_app/static/defaults/bg.jpg'
nb_landmarks = 5
SHAPE = 256
gauss_std = 0.05
ist = np.array([[[[0.]]]])

STYLE_DIM = 8
STYLE_DIM_2 = 8

# (RAND Z VECTOR -> LANDMARKS) MODEL
with tf.gfile.GFile(z_to_l_model, "rb") as f:
    graph_def = tf.GraphDef()
    graph_def.ParseFromString(f.read())
with tf.Graph().as_default() as graph_z_to_l:
    tf.import_graph_def(graph_def, name='')

# (LANDMARKS -> MASK) MODEL
with tf.gfile.GFile(l_to_m_model, "rb") as f:
    graph_def = tf.GraphDef()
    graph_def.ParseFromString(f.read())
with tf.Graph().as_default() as graph_l_to_m:
    tf.import_graph_def(graph_def, name='')

# (MASK -> TEXTURE) MODEL
with tf.gfile.GFile(m_to_t_model, "rb") as f:
    graph_def = tf.GraphDef()
    a =graph_def.ParseFromString(f.read())
with tf.Graph().as_default() as graph_m_to_t:
    tf.import_graph_def(graph_def, name='')

# We access the input and output nodes
mu_lm = graph_l_to_m.get_tensor_by_name('gen/genlandmarks/Reshape:0')
sigmax_lm = graph_l_to_m.get_tensor_by_name('gen/genlandmarks/sigmax:0')
sigmay_lm = graph_l_to_m.get_tensor_by_name('gen/genlandmarks/sigmay:0')
cov_lm = graph_l_to_m.get_tensor_by_name('gen/genlandmarks/mul_3:0')
mu_zl = graph_z_to_l.get_tensor_by_name('gen/genlandmarks/Reshape:0')
sigmax_zl = graph_z_to_l.get_tensor_by_name('gen/genlandmarks/sigmax:0')
sigmay_zl = graph_z_to_l.get_tensor_by_name('gen/genlandmarks/sigmay:0')
cov_zl = graph_z_to_l.get_tensor_by_name('gen/genlandmarks/mul_3:0')

zgt = graph_z_to_l.get_tensor_by_name('gen/senc/add:0')
z = graph_l_to_m.get_tensor_by_name('gen/senc_m/add:0')
bbx = graph_z_to_l.get_tensor_by_name('bbx:0')
genm = graph_l_to_m.get_tensor_by_name('gen/genmask/convlast/output:0')

mu_mt = graph_m_to_t.get_tensor_by_name('gen/genlandmarks/Reshape:0')
sigmax_mt = graph_m_to_t.get_tensor_by_name('gen/genlandmarks/sigmax:0')
sigmay_mt = graph_m_to_t.get_tensor_by_name('gen/genlandmarks/sigmay:0')
cov_mt = graph_m_to_t.get_tensor_by_name('gen/genlandmarks/mul_3:0')
im_rgb = graph_m_to_t.get_tensor_by_name('input:0')
mask = graph_m_to_t.get_tensor_by_name('mask:0')
z2 = graph_m_to_t.get_tensor_by_name('gen/senc_t/add:0')
genim = graph_m_to_t.get_tensor_by_name('gen/gent/add:0')
istrain = graph_m_to_t.get_tensor_by_name('istrain:0')

def gen_gaussians(mu_xs, mu_ys, sigma_xs, sigma_ys, covs, gaussian_colors):
    """
    Generate colorzed gaussian map image from sum of gaussian parameters.
    """

    mu_zl_np = np.array([np.stack([mu_xs, mu_ys], axis=-1)])
    sigmax_zl_np = np.array([sigma_xs])
    sigmay_zl_np = np.array([sigma_ys])
    cov_zl_np = np.array([covs])

    gm = get_gaussian_maps(mu_zl_np, sigmax_zl_np, sigmay_zl_np, cov_zl_np, [SHAPE, SHAPE])
    gm = colorize_landmark_maps(gm[0], gaussian_colors)
    gm = np.squeeze(gm)

    return gm


def sample_l(bbx_region, gaussian_colors):
    """
    Generate landmarks from bounding box info.
    """

    _, _, _, r, _, _, z_np, _, _ = \
            replacement_inference_cs_kas_draw_bbx(orig_im, bbx_region, SHAPE, STYLE_DIM, STYLE_DIM_2)

    with tf.Session(graph=graph_z_to_l) as sess_z_to_l:
        [mu_zl_np, sigmax_zl_np, sigmay_zl_np, cov_zl_np] = sess_z_to_l.run([mu_zl, sigmax_zl, sigmay_zl, cov_zl],
                                                                            feed_dict={zgt: z_np, bbx: r})
        sess_z_to_l.close()

    gm = get_gaussian_maps(mu_zl_np, sigmax_zl_np, sigmay_zl_np, cov_zl_np, [SHAPE, SHAPE])
    gm = colorize_landmark_maps(gm[0], gaussian_colors)
    gm = np.squeeze(gm)

    return gm, mu_zl_np[..., 0], mu_zl_np[..., 1], sigmax_zl_np, sigmay_zl_np, cov_zl_np


def run_models(bbx_region, mu_xs, mu_ys, sigma_xs, sigma_ys, covs):
    """
    Run only the model generation for the program, not gaussian map generation.
    """
    mu_zl_np = np.array([np.stack([mu_xs, mu_ys], axis=-1)])
    sigmax_zl_np = np.array([sigma_xs])
    sigmay_zl_np = np.array([sigma_ys])
    cov_zl_np = np.array([covs])

    np_im, template, np_m, r, np_im_orig, box_, z_np, z2_np, z3_np = \
            replacement_inference_cs_kas_draw_bbx(orig_im, bbx_region, SHAPE, STYLE_DIM, STYLE_DIM_2)
    np_im_orig_cp = np_im_orig.copy()

    with tf.Session(graph=graph_l_to_m) as sess_l_to_m:
        m_final = sess_l_to_m.run(genm, feed_dict={
            mu_lm: mu_zl_np,
            sigmax_lm:sigmax_zl_np,
            sigmay_lm:sigmay_zl_np,
            cov_lm:cov_zl_np, z: z2_np})
        sess_l_to_m.close()

    sigma = np.stack([sigmax_zl_np ** 2, cov_zl_np, cov_zl_np, sigmay_zl_np ** 2], axis=-1)
    sigma = np.reshape(sigma, [-1, 2,2])
    u,s,v = np.linalg.svd(sigma)
    angles = np.arccos(u[:, 1, 0])
    ssqrts  = np.sqrt(s)

    rorig_ = np.squeeze((mu_zl_np+1)*0.5)
    m_final = (np.squeeze((m_final + 1) * 0.5 * 255)).astype(np.uint8)
    m_final_ = cv2.resize(m_final, (box_[3] - box_[1], box_[2] - box_[0]))
    cvs = np.zeros([SHAPE, SHAPE])
    cvs[box_[0]:box_[2], box_[1]:box_[3]] = m_final_
    with tf.Session(graph=graph_m_to_t) as sess_m_to_t:
        im_final = sess_m_to_t.run(genim, feed_dict={
            im_rgb: np_im,
            mask: m_final[None, :, :, None],
            mu_mt: mu_zl_np, sigmax_mt: sigmax_zl_np, sigmay_mt: sigmay_zl_np,
            cov_mt: cov_zl_np,
            z2: z3_np,
            istrain: ist})
        sess_m_to_t.close()

    im_final = (im_final + 1) * 0.5 * 255
    im_final = im_final.squeeze().astype(np.uint8)

    im_final_ = cv2.resize(im_final, (box_[3] - box_[1], box_[2] - box_[0]))
    np_im_orig_cp[box_[0]:box_[2], box_[1]:box_[3], :] = im_final_
    np_im_orig_cp = np.squeeze(np_im_orig_cp)

    return m_final, np_im_orig_cp


def run_all(bbx_region, gaussian_colors):
    np_im, template, np_m, r, np_im_orig, box_, z_np, z2_np, z3_np = \
            replacement_inference_cs_kas_draw_bbx(orig_im, bbx_region, SHAPE, STYLE_DIM, STYLE_DIM_2)

    np_im_orig_cp = np_im_orig.copy()
    with tf.Session(graph=graph_z_to_l) as sess_z_to_l:
        [mu_zl_np, sigmax_zl_np, sigmay_zl_np, cov_zl_np] = \
                sess_z_to_l.run([mu_zl, sigmax_zl, sigmay_zl, cov_zl], feed_dict={zgt : z_np, bbx:r})
        sess_z_to_l.close()

    with tf.Session(graph=graph_l_to_m) as sess_l_to_m:
        m_final = sess_l_to_m.run(genm, feed_dict={
            mu_lm: mu_zl_np,
            sigmax_lm:sigmax_zl_np,
            sigmay_lm:sigmay_zl_np,
            cov_lm:cov_zl_np, z: z2_np})
        sess_l_to_m.close()

    sigma = np.stack([sigmax_zl_np ** 2, cov_zl_np, cov_zl_np, sigmay_zl_np ** 2], axis=-1)
    sigma = np.reshape(sigma, [-1, 2,2])
    u,s,v = np.linalg.svd(sigma)
    angles = np.arccos(u[:, 1, 0])
    ssqrts  = np.sqrt(s)

    rorig_ = np.squeeze((mu_zl_np+1)*0.5)
    m_final = (np.squeeze((m_final + 1) * 0.5 * 255)).astype(np.uint8)
    m_final_ = cv2.resize(m_final, (box_[3] - box_[1], box_[2] - box_[0]))
    cvs = np.zeros([SHAPE, SHAPE])
    cvs[box_[0]:box_[2], box_[1]:box_[3]] = m_final_
    with tf.Session(graph=graph_m_to_t) as sess_m_to_t:
        im_final = sess_m_to_t.run(genim, feed_dict={
            im_rgb: np_im,
            mask: m_final[None, :, :, None],
            mu_mt: mu_zl_np, sigmax_mt: sigmax_zl_np, sigmay_mt: sigmay_zl_np,
            cov_mt: cov_zl_np,
            z2: z3_np,
            istrain: ist})
        sess_m_to_t.close()

    im_final = (im_final + 1) * 0.5 * 255
    im_final = im_final.squeeze().astype(np.uint8)

    im_final_ = cv2.resize(im_final, (box_[3] - box_[1], box_[2] - box_[0]))
    np_im_orig_cp[box_[0]:box_[2], box_[1]:box_[3], :] = im_final_

    x = rorig_[:,0]*SHAPE
    yvals = rorig_[:,1]*SHAPE
    cvs = np.zeros([SHAPE, SHAPE, 3])
    gm = get_gaussian_maps(mu_zl_np, sigmax_zl_np, sigmay_zl_np, cov_zl_np, [SHAPE, SHAPE])
    gm = colorize_landmark_maps(gm[0], gaussian_colors)
    gm = np.squeeze(gm)
    np_im_orig_cp = np.squeeze(np_im_orig_cp)

    return gm, mu_zl_np[..., 0], mu_zl_np[..., 1], \
            sigmax_zl_np, sigmay_zl_np, cov_zl_np, \
            m_final, np_im_orig_cp


# taken from https://github.com/tomasjakab/imm/
def get_gaussian_maps(mu, sigmax, sigmay, covs, shape_hw, mode='rot'):
    """
    Generates [B,SHAPE_H,SHAPE_W,NMAPS] tensor of 2D gaussians,
    given the gaussian centers: MU [B, NMAPS, 2] tensor.

    STD: is the fixed standard dev.
    """
    with tf.name_scope(None, 'gauss_map', [mu]):
        y = tf.to_float(tf.linspace(-1.0, 1.0, shape_hw[0]))
        x = tf.to_float(tf.linspace(-1.0, 1.0, shape_hw[1]))
        [x,y] = tf.meshgrid(x,y)
        xy = tf.stack([x, y], axis=-1)
        xy = tf.stack([xy] * nb_landmarks, axis=0)
        xy = xy[None, : ,:, :, :]
    if mode in ['rot', 'flat']:
        mu = mu[:,:,None, None,:]

        invsigma = tf.stack([sigmay**2, -covs, -covs, sigmax**2], axis=-1)
        invsigma = tf.reshape(invsigma, [-1, nb_landmarks, 2,2])
        denominator = (sigmax*sigmay)**2 - covs**2
        denominator = tf.expand_dims(tf.expand_dims(denominator, -1), -1)
        invsigma = invsigma/(denominator)
        invsigma = tf.cast(invsigma, tf.float32)
        pp = tf.tile(invsigma[:, :, None, :, :], [1, 1, shape_hw[1], 1, 1])
        X = xy-mu
        dist = tf.matmul(X,pp)
        dist = tf.reduce_sum((dist*X), axis=-1)

        if mode == 'rot':
            g_yx = tf.exp(-dist)
        else:
            g_yx = tf.exp(-tf.pow(dist + 1e-5, 0.25))

    else:
        raise ValueError('Unknown mode: ' + str(mode))

    g_yx = tf.transpose(g_yx, perm=[0, 2, 3, 1])
    return g_yx

# get "maximally" different random colors:
#  ref: https://gist.github.com/adewes/5884820
def get_random_color(pastel_factor = 0.5):
    return [(x+pastel_factor)/(1.0+pastel_factor) for x in [random.uniform(0,1.0) for i in [1,2,3]]]

def color_distance(c1,c2):
    return sum([abs(x[0]-x[1]) for x in zip(c1,c2)])

def generate_new_color(existing_colors, pastel_factor=0.5):
    max_distance = None
    best_color = None
    for i in range(0,100):
        color = get_random_color(pastel_factor = pastel_factor)
        if not existing_colors:
            return color
        best_distance = min([color_distance(color,c) for c in existing_colors])
        if not max_distance or best_distance > max_distance:
            max_distance = best_distance
            best_color = color
    return best_color

def get_n_colors(n, pastel_factor=0.9):
    colors = []
    for i in range(n):
        colors.append(generate_new_color(colors,pastel_factor = 0.9))
    return colors

COLORS = get_n_colors(nb_landmarks, pastel_factor=0.9)

def colorize_landmark_maps(maps, color_list=COLORS):
    """
    Given BxHxWxN maps of landmarks, returns an aggregated landmark map
    in which each landmark is colored randomly. BxHxWxN
    """
    n_maps = maps.shape[-1]
    hmaps = [np.expand_dims(maps[..., i], axis=2) * np.reshape(color_list[i], [1, 1, 1, 3])
           for i in range(n_maps)]
    return np.max(hmaps, axis=0)
