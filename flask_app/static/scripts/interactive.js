/**
 * =======================================
 * BOUNDING BOX INTERACTION
 * =======================================
 */

var bg_img_height = 0;
var bg_img_width =  0;

var bg_img_canvas = document.querySelector("#bg-image"),
    bg_img_context = bg_img_canvas.getContext("2d");

var bg_img = new Image();
bg_img.src = bg_img_path;
bg_img_context.drawImage(bg_img, 0, 0);
bg_img.onload = function(){
    bg_img_height = bg_img.height;
    bg_img_width = bg_img.width;
    bg_img_context.drawImage(bg_img, 0, 0);
}

var bbx = {},
    bbx_scale = false,
    bbx_move = false;

function init_bg_interaction() {
    bg_img_canvas.addEventListener('mousedown', bbxMouseDown, false);
    bg_img_canvas.addEventListener('mouseup', bbxMouseUp, false);
    bg_img_canvas.addEventListener('mousemove', bbxMouseMove, false);
}

function in_bbx(e) {
    if (bbx.start_x == null || bbx.start_y == null || bbx.w == null || bbx.h == null) {
        return false;
    }

    var bounds_x = [
        Math.min(bbx.start_x, bbx.start_x + bbx.w),
        Math.max(bbx.start_x, bbx.start_x + bbx.w)
    ];
    var bounds_y = [
        Math.min(bbx.start_y, bbx.start_y + bbx.h),
        Math.max(bbx.start_y, bbx.start_y + bbx.h)
    ];

    if (e.offsetX < bounds_x[0] || e.offsetX > bounds_x[1]) {
        return false;
    }
    if (e.offsetY < bounds_y[0] || e.offsetY > bounds_y[1]) {
        return false;
    }
    return true;
}

function bbxMouseDown(e) {
    if (in_bbx(e)) {
        bbx.mouse_down_x = e.offsetX;
        bbx.mouse_down_y = e.offsetY;
        bbx_move = true;
        bbx.color = "#000000";
        bbx_draw();
    } else {
        bbx.w = 0;
        bbx.h = 0;
        bbx.start_x = e.offsetX;
        bbx.start_y = e.offsetY;
        bbx_scale = true;
        bbx.color = "#000000";
        bbx_draw();
    }
}

function bbxMouseUp() {
    bbx_scale = false;
    bbx_move = false;
    bbx.color = "#000000";
    if (bbx.w < 0) {
        bbx.start_x = bbx.start_x + bbx.w;
        bbx.w = -bbx.w;
    }
    if (bbx.h < 0) {
        bbx.start_y = bbx.start_y + bbx.h;
        bbx.h = -bbx.h;
    }

    // Clamp to image boundaries
    if (bbx.start_x < 0) {
        bbx.w += bbx.start_x;
        bbx.start_x = 0;
    }
    if (bbx.start_y < 0) {
        bbx.h += bbx.start_y;
        bbx.start_y = 0;
    }
    if (bbx.start_x + bbx.w >= bg_img_width) {
        bbx.w = bg_img_width - bbx.start_x - 1;
    }
    if (bbx.start_y + bbx.h >= bg_img_height) {
        bbx.h = bg_img_height - bbx.start_y - 1;
    }
    bbx_draw();
}

function bbxMouseMove(e) {
    if (bbx_scale) {
        bbx.w = e.offsetX - bbx.start_x;
        bbx.h = e.offsetY - bbx.start_y ;
        bbx_draw();
    } else if (bbx_move) {
        bbx.start_x += e.offsetX - bbx.mouse_down_x;
        bbx.start_y += e.offsetY - bbx.mouse_down_y;
        bbx.mouse_down_x = e.offsetX;
        bbx.mouse_down_y = e.offsetY;
        bbx_draw();
    } else if (in_bbx(e)) {
        bg_img_canvas.style.cursor = "move"; 
    } else {
        bg_img_canvas.style.cursor = "crosshair"; 
    }

}

function bbx_draw() {
    bg_img_context.clearRect(0,0,bg_img_canvas.width,bg_img_canvas.height);
    bg_img_context.drawImage(bg_img, 0, 0);
    bg_img_context.lineWidth = 4;

    bg_img_context.strokeStyle = bbx.color;
    bg_img_context.strokeRect(bbx.start_x, bbx.start_y, bbx.w, bbx.h);
}

init_bg_interaction();

/**
 * =======================================
 * GAUSSIANS INTERACTION
 * =======================================
 */

var gl_canvas = document.querySelector("#ellipsis"),
    gl_width = gl_canvas.width,
    gl_height = gl_canvas.height,
    gl_mode = "translate";

let n = mu_xs.length // Number of gaussian landmarks
let radius = 3

function initGaussianColors(pastel_factor) {

    var gaussian_colors = [];

    function color_distance(color_1, color_2) {
        var accumulator = 0;
        for (c = 0; c < 3; c++) {
            accumulator += Math.abs(color_2[c] - color_1[c]);
        }
        return accumulator;
    }

    for (var g_idx = 0; g_idx < n; g_idx++) {

        var max_distance = null
        var best_color = null

        for (var i = 0; i < 100; i++) {
            var rand_color = [0, 0, 0];

            for (var c = 0; c < 3; c++) {
                var rand_val = Math.random();
                rand_color[c] = (rand_val + pastel_factor) / (1.0 + pastel_factor);
            }

            if (gaussian_colors.length == 0) {
                best_color = rand_color;
                break;
            } else {
                var cur_min_distance = Infinity;

                // Find distance of rand_color to closest color in existing gaussian_colors list
                for (var c2 = 0; c2 < gaussian_colors.length; c2++) {
                    var cur_dist = color_distance(gaussian_colors[c2], rand_color);
                    if (cur_dist < cur_min_distance) {
                        cur_min_distance = cur_dist;
                    }
                }

                if (max_distance < cur_min_distance || max_distance == null) {
                    best_color = rand_color;
                    max_distance = cur_min_distance;
                }
            }
        }

        gaussian_colors.push(best_color);
        gaussians[g_idx].color = best_color;
    }

}

// Translate pixel coordinate to [-1, 1]x[-1, 1] space
function pixelToGaussianSpace(x, y) {
    var x_prime = (2 * (x / gl_canvas.width)) - 1;
    var y_prime = (2 * (y / gl_canvas.height)) - 1;

    return [x_prime, y_prime]
}

// Translate coordinate in gaussian space to pixel coordinate
function gaussianToPixelSpace(x_prime, y_prime) {
    var x = ((x_prime + 1) / 2) * width;
    var y = ((y_prime + 1) / 2) * height;

    return [x, y]
}

function calcTransformParams(gaussian) {
    var a = gaussian.sigma_x * gaussian.sigma_x;
    var b = gaussian.cov;
    var c = gaussian.sigma_y * gaussian.sigma_y;

    var l_term = (a+c) / 2.0;
    var r_term = Math.sqrt(Math.pow((a-c)/2.0, 2.0) + (b*b));

    var lambda_x = Math.sqrt(l_term + r_term);
    var lambda_y = Math.sqrt(l_term - r_term);

    var theta = 0;
    if (b == 0 && a < c) {
        theta = Math.PI / 2.0;
    } else if (b != 0) {
        theta = Math.atan2(lambda_x - a, b);
    }

    gaussian.lambda_x = lambda_x;
    gaussian.lambda_y = lambda_y;
    gaussian.theta = theta;
}

function calcGaussianParams(gaussian) {
    let lambda_x_sq = gaussian.lambda_x * gaussian.lambda_x;
    let lambda_y_sq = gaussian.lambda_y * gaussian.lambda_y;
    let sin_theta = Math.sin(gaussian.theta);
    let sin_theta_sq = sin_theta * sin_theta;
    let cos_theta = Math.cos(gaussian.theta);
    let cos_theta_sq = cos_theta * cos_theta;

    gaussian.sigma_x = Math.sqrt((lambda_x_sq * cos_theta_sq) + (lambda_y_sq * sin_theta_sq));
    gaussian.sigma_y = Math.sqrt((lambda_x_sq * sin_theta_sq) + (lambda_y_sq * cos_theta_sq));
    gaussian.cov = (lambda_x_sq - lambda_y_sq) * sin_theta * cos_theta;
}

const gaussians = d3.range(n).map(i => ({
    // Bivariate gaussian parameters:
    mu_x: mu_xs[i],
    mu_y: mu_ys[i],
    sigma_x: sigma_xs[i],
    sigma_y: sigma_ys[i],
    cov: covs[i],
    // Interactive transformations parameters:
    pix_x: ((mu_xs[i] + 1) / 2) * gl_canvas.width,
    pix_y: ((mu_ys[i] + 1) / 2) * gl_canvas.height,
    lambda_x: 0,
    lambda_y: 0,
    theta: 0,
    // Graphical parameters:
    color: [0, 0, 0],
    active: false
}));

for (var i = 0; i < gaussians.length; i++) {
    calcTransformParams(gaussians[i]);
}

// Initialize gaussian colors
initGaussianColors(0.5);

function drag() {
    // Choose the circle that is closest to the pointer for dragging.
    function closestSubject() {
        let subject = null;
        let distance = Infinity;
        for (const g of gaussians) {
            let d = Math.hypot(d3.mouse(this)[0] - g.pix_x, d3.mouse(this)[1] - g.pix_y);
            if (d < distance) {
                distance = d;
                subject = g;
            }
        }
        return subject;
    }

    function dragstarted() {
        d3.event.subject.active = true;
    }

    function dragged() {
        if (gl_mode == "translate") {
            d3.event.subject.pix_x += d3.event.dx;
            d3.event.subject.pix_y += d3.event.dy;
            var newMu = pixelToGaussianSpace(
                d3.event.subject.pix_x,
                d3.event.subject.pix_y
            );
            d3.event.subject.mu_x = newMu[0];
            d3.event.subject.mu_y = newMu[1];
        } else if (gl_mode == "rotate") {
            d3.event.subject.theta += 0.02 * d3.event.dy;

            if (d3.event.subject.theta > Math.PI) {
                d3.event.subject.theta = d3.event.subject.theta - Math.PI;
            }
            if (d3.event.subject.theta < -Math.PI) {
                d3.event.subject.theta = d3.event.subject.theta + Math.PI;
            }
            calcGaussianParams(d3.event.subject);
        } else if (gl_mode == "scale") {
            d3.event.subject.lambda_x += Math.abs(Math.cos(d3.event.subject.theta)) 
                * (d3.event.dx / (0.5 * gl_canvas.width));
            d3.event.subject.lambda_x += Math.abs(Math.sin(d3.event.subject.theta))
                * (d3.event.dy / (0.5 * gl_canvas.height));

            d3.event.subject.lambda_y += Math.abs(Math.sin(d3.event.subject.theta))
                * (d3.event.dx / (0.5 * gl_canvas.width));
            d3.event.subject.lambda_y += Math.abs(Math.cos(d3.event.subject.theta))
                * (d3.event.dy / (0.5 * gl_canvas.height));
            calcGaussianParams(d3.event.subject);
        }

        render();
    }

    function dragended() {
        d3.event.subject.active = false;
        fetchGaussians();

        console.log(d3.event.subject);
    }

    return d3.drag()
        .subject(closestSubject)
        .on("start", dragstarted)
        .on("drag", dragged)
        .on("end", dragended)
};

d3.selectAll("#ellipsis").call(drag());

var gl = gl_canvas.getContext("webgl");

function initGL() {
    var num_gaussians = gaussians.length;

    if (gl == null) {
        console.log("WEBGL COULD NOT BE ENABLED");
        return;
    }

    var vertShaderCode = `
        attribute vec2 vertexPositionNDC;
        void main() {
            gl_Position = vec4(vertexPositionNDC, 0.0, 1.0);
        }
    `;

    var vertShader = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertShader, vertShaderCode);
    gl.compileShader(vertShader);

    var fragShaderCode = `
        precision mediump float;

        uniform float colors[${num_gaussians * 3}];
        uniform float mu_xs[${num_gaussians}];
        uniform float mu_ys[${num_gaussians}];
        uniform float sigma_xs[${num_gaussians}];
        uniform float sigma_ys[${num_gaussians}];
        uniform float covs[${num_gaussians}];

        const float M_PI = 3.141592653;

        void main() {
            vec3 max_color = vec3(0, 0, 0);

            vec2 cur_resolution = vec2(${gl_canvas.width}, ${gl_canvas.height});

            vec2 loc = (2.0 * gl_FragCoord.xy / cur_resolution.xy) - vec2(1.0, 1.0);
            loc.y = -loc.y;

            for (int i = 0; i < ${num_gaussians}; i++) {
                float x_var = sigma_xs[i] * sigma_xs[i];
                float y_var = sigma_ys[i] * sigma_ys[i];
                float cov = covs[i];

                vec2 mu = vec2(mu_xs[i], mu_ys[i]);

                mat2 cov_inv = mat2(
                    y_var, -cov,
                    -cov, x_var
                );
                cov_inv = cov_inv / ((x_var * y_var) - (cov * cov));

                vec2 loc_mu = loc - mu;
                float exp_term = dot(loc_mu, cov_inv * loc_mu);
                exp_term = exp(-exp_term);

                vec3 cur_color = vec3(
                    colors[i*3] * exp_term,
                    colors[i*3 + 1] * exp_term,
                    colors[i*3 + 2] * exp_term
                );

                max_color.r = max(max_color.r, cur_color.r);
                max_color.g = max(max_color.g, cur_color.g);
                max_color.b = max(max_color.b, cur_color.b);
            }

            gl_FragColor = vec4(max_color, 1.0);
        }
    `;

    var fragShader = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragShader, fragShaderCode);
    gl.compileShader(fragShader);

    var prgm = gl.createProgram();
    gl.attachShader(prgm, vertShader);
    gl.attachShader(prgm, fragShader);
    gl.linkProgram(prgm);
    gl.useProgram(prgm);

    return prgm;
}

var shaderProgram = initGL();

function render() {
    var gaussian_colors = [];
    var gaussian_mu_xs = [];
    var gaussian_mu_ys = [];
    var gaussian_sigma_xs = [];
    var gaussian_sigma_ys = [];
    var gaussian_covs = [];

    for (var i = 0; i < gaussians.length; i++) {
        gaussian_colors = gaussian_colors.concat(gaussians[i].color);
        gaussian_mu_xs = gaussian_mu_xs.concat(gaussians[i].mu_x);
        gaussian_mu_ys = gaussian_mu_ys.concat(gaussians[i].mu_y);
        gaussian_sigma_xs = gaussian_sigma_xs.concat(gaussians[i].sigma_x);
        gaussian_sigma_ys = gaussian_sigma_ys.concat(gaussians[i].sigma_y);
        gaussian_covs = gaussian_covs.concat(gaussians[i].cov);
    }

    if (gl == null) {
        console.log("WEBGL COULD NOT BE ENABLED");
        return;
    }

    var quadVerts = [
         1.0,  1.0,
        -1.0,  1.0,
        -1.0, -1.0,
        -1.0, -1.0,
         1.0, -1.0,
         1.0,  1.0
    ];

    var quadVertsBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, quadVertsBuffer);

    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(quadVerts), gl.STATIC_DRAW);
    var coord = gl.getAttribLocation(shaderProgram, "vertexPositionNDC");
    gl.vertexAttribPointer(coord, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(coord);

    var uColors = gl.getUniformLocation(shaderProgram, "colors");
    gl.uniform1fv(uColors, new Float32Array(gaussian_colors));
    var uMuXs = gl.getUniformLocation(shaderProgram, "mu_xs");
    gl.uniform1fv(uMuXs, new Float32Array(gaussian_mu_xs));
    var uMuYs = gl.getUniformLocation(shaderProgram, "mu_ys");
    gl.uniform1fv(uMuYs, new Float32Array(gaussian_mu_ys));
    var uSigmaXs = gl.getUniformLocation(shaderProgram, "sigma_xs");
    gl.uniform1fv(uSigmaXs, new Float32Array(gaussian_sigma_xs));
    var uSigmaYs = gl.getUniformLocation(shaderProgram, "sigma_ys");
    gl.uniform1fv(uSigmaYs, new Float32Array(gaussian_sigma_ys));
    var uCovs = gl.getUniformLocation(shaderProgram, "covs");
    gl.uniform1fv(uCovs, new Float32Array(gaussian_covs));

    gl.clearColor(0, 0, 0, 1);
    gl.enable(gl.DEPTH_TEST);
    gl.clear(gl.COLOR_BUFFER_BIT);
    gl.viewport(0, 0, gl_canvas.width, gl_canvas.height);
    gl.drawArrays(gl.TRIANGLES, 0, 6);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
}


// Key bindings
d3.select("body").on("keydown", function() {
    if (d3.event.keyCode == 16) {
        gl_mode = "scale";
    } else if (d3.event.keyCode == 18) {
        gl_mode = "rotate"
    }
});
d3.select("body").on("keyup", function() {
    if (d3.event.keyCode == 16 || d3.event.keyCode == 18) {
        gl_mode = "translate";
    }
});
d3.select("body").on("keypress", function() {
    if (d3.event.keyCode == 13) {
        fetchModelRuns();
    }
});

// Button bindings
d3.select("#gen-landmarks-button").on("click", function() {
    fetchLandmarks();
});
d3.select("#gen-mask-and-texture-button").on("click", function() {
    fetchModelRuns();
});
d3.select("#gen-all-button").on("click", function() {
    fetchAll();
});



function fetchLandmarks() {
    var generation_info = getGenerationInfo();

    fetch("/api/run_landmarks", {
        method: 'POST',
        headers: {'Content-Type': 'application/json;charset=utf-8'},
        body: JSON.stringify(generation_info)
    }).then(function (response) {
        return response.json();
    }).then(function(json_response) {

        // Update gaussian parameters
        var mu_xs = json_response.mu_xs;
        var mu_ys = json_response.mu_ys;
        var sigma_xs = json_response.sigma_xs;
        var sigma_ys = json_response.sigma_ys;
        var covs = json_response.covs;

        for (var i = 0; i < gaussians.length; i++) {
            gaussians[i].pix_x = ((mu_xs[i] + 1) / 2) * gl_canvas.width,
            gaussians[i].pix_y = ((mu_ys[i] + 1) / 2) * gl_canvas.height,
            gaussians[i].mu_x = mu_xs[i];
            gaussians[i].mu_y = mu_ys[i];
            gaussians[i].sigma_x = sigma_xs[i];
            gaussians[i].sigma_y = sigma_ys[i];
            gaussians[i].cov = covs[i];
        }

        render();

        // Update gaussian image
        var gauss_maps_base64 = json_response.gauss_maps;
        var gauss_maps_canvas = document.querySelector("#gaussians"),
            gauss_maps_context = gauss_maps_canvas.getContext("2d");

        var gauss_img = new Image();
        gauss_img.src = "data:image/png;base64," + gauss_maps_base64;
        gauss_img.onload = function(){
            gauss_maps_context.drawImage(gauss_img, 0, 0);
        };

    });
}

function fetchGaussians() {
    var generation_info = getGenerationInfo();

    fetch("/api/run_gaussians", {
        method: 'POST',
        headers: {'Content-Type': 'application/json;charset=utf-8'},
        body: JSON.stringify(generation_info)
    }).then(function (response) {
        return response.json();
    }).then(function(json_response) {
        var gauss_maps_base64 = json_response.gauss_maps;

        var gauss_maps_canvas = document.querySelector("#gaussians"),
            gauss_maps_context = gauss_maps_canvas.getContext("2d");

        var gauss_img = new Image();
        gauss_img.src = "data:image/png;base64," + gauss_maps_base64;
        gauss_img.onload = function(){
            gauss_maps_context.drawImage(gauss_img, 0, 0);
        };
    });
}

function fetchModelRuns() {
    var loader_divs = document.querySelectorAll("#mask-frame .loader-container")
    setLoading(loader_divs);
    loader_divs = document.querySelectorAll("#texture-frame .loader-container");
    setLoading(loader_divs);
    if (!bbx.start_x) {
        return;
    }

    var mask_canvas = document.querySelector("#mask"),
        mask_context = mask_canvas.getContext("2d");

    var texture_canvas = document.querySelector("#texture"),
        texture_context = texture_canvas.getContext("2d");

    var generation_info = getGenerationInfo();

    fetch("/api/run_models", {
        method: 'POST',
        headers: {'Content-Type': 'application/json;charset=utf-8'},
        body: JSON.stringify(generation_info)
    }).then(function(response) {
        return response.json();
    }).then(function(json_response) {
        var mask_img_base64 = json_response.mask_img;
        var texture_img_base64 = json_response.texture_img;

        var mask_img = new Image();
        mask_img.src = "data:image/png;base64," + mask_img_base64;
        mask_img.onload = function(){
            mask_context.drawImage(mask_img, 0, 0);
            endLoading(document.querySelectorAll("#mask-frame .loader-container"));
        };

        var texture_img = new Image();
        texture_img.src = "data:image/png;base64," + texture_img_base64;
        texture_img.onload = function(){
            texture_context.drawImage(texture_img, 0, 0);
            endLoading(document.querySelectorAll("#texture-frame .loader-container"));
        };
    });
}

function fetchAll() {
    var loader_divs = document.querySelectorAll(".loader-container")
    setLoading(loader_divs);
    if (!bbx.start_x) {
        return;
    }

    var gauss_maps_canvas = document.querySelector("#gaussians"),
        gauss_maps_context = gauss_maps_canvas.getContext("2d");

    var mask_canvas = document.querySelector("#mask"),
        mask_context = mask_canvas.getContext("2d");

    var texture_canvas = document.querySelector("#texture"),
        texture_context = texture_canvas.getContext("2d");

    var generation_info = getGenerationInfo();

    fetch("/api/run_all", {
        method: 'POST',
        headers: {'Content-Type': 'application/json;charset=utf-8'},
        body: JSON.stringify(generation_info)
    }).then(function(response) {
        return response.json();
    }).then(function(json_response) {
        var gauss_maps_base64 = json_response.gauss_maps;
        var mask_img_base64 = json_response.mask_img;
        var texture_img_base64 = json_response.texture_img;

        // Update gaussian parameters
        var mu_xs = json_response.mu_xs;
        var mu_ys = json_response.mu_ys;
        var sigma_xs = json_response.sigma_xs;
        var sigma_ys = json_response.sigma_ys;
        var covs = json_response.covs;

        for (var i = 0; i < gaussians.length; i++) {
            gaussians[i].pix_x = ((mu_xs[i] + 1) / 2) * gl_canvas.width,
            gaussians[i].pix_y = ((mu_ys[i] + 1) / 2) * gl_canvas.height,
            gaussians[i].mu_x = mu_xs[i];
            gaussians[i].mu_y = mu_ys[i];
            gaussians[i].sigma_x = sigma_xs[i];
            gaussians[i].sigma_y = sigma_ys[i];
            gaussians[i].cov = covs[i];
        }

        render();

        var gauss_img = new Image();
        gauss_img.src = "data:image/png;base64," + gauss_maps_base64;
        gauss_img.onload = function(){
            gauss_maps_context.drawImage(gauss_img, 0, 0);
            endLoading(document.querySelectorAll("#ellipsis-frame .loader-container"));
            endLoading(document.querySelectorAll("#gaussians-frame .loader-container"));
        };

        var mask_img = new Image();
        mask_img.src = "data:image/png;base64," + mask_img_base64;
        mask_img.onload = function(){
            mask_context.drawImage(mask_img, 0, 0);
            endLoading(document.querySelectorAll("#mask-frame .loader-container"));
        };

        var texture_img = new Image();
        texture_img.src = "data:image/png;base64," + texture_img_base64;
        texture_img.onload = function(){
            texture_context.drawImage(texture_img, 0, 0);
            endLoading(document.querySelectorAll("#texture-frame .loader-container"));
        };
    });
}

function setLoading(loader_divs) {
    for (var i = 0; i < loader_divs.length; i++) {
        loader_divs[i].style.display = "flex";
    }
}

function endLoading(loader_divs) {
    for (var i = 0; i < loader_divs.length; i++) {
        loader_divs[i].style.display = "none";
    }
}

function getGenerationInfo() {
    var gaussian_mu_xs = [];
    var gaussian_mu_ys = [];
    var gaussian_sigma_xs = [];
    var gaussian_sigma_ys = [];
    var gaussian_covs = [];
    var cur_colors = [];

    for (var i = 0; i < gaussians.length; i++) {
        gaussian_mu_xs.push(gaussians[i].mu_x);
        gaussian_mu_ys.push(gaussians[i].mu_y);
        gaussian_sigma_xs.push(gaussians[i].sigma_x);
        gaussian_sigma_ys.push(gaussians[i].sigma_y);
        gaussian_covs.push(gaussians[i].cov);
        cur_colors.push(gaussians[i].color);
    }

    var generation_info = {
        bbx_region: [bbx.start_x, bbx.start_y, bbx.w, bbx.h],
        gaussian_mu_xs: gaussian_mu_xs,
        gaussian_mu_ys: gaussian_mu_ys,
        gaussian_sigma_xs: gaussian_sigma_xs,
        gaussian_sigma_ys: gaussian_sigma_ys,
        gaussian_covs: gaussian_covs,
        gaussian_colors: cur_colors
    };

    return generation_info;
}

// Render the initial canvas.
render();
fetchGaussians();
