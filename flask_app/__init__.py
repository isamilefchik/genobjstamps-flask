"""
Initialization for Flask app.
"""

import time
import os
import numpy as np
from flask import Flask, render_template, request, jsonify, \
        redirect, url_for, send_from_directory
from PIL import Image
import flask_app.model_runner as model_runner
import base64
from io import BytesIO

app = Flask(__name__)

@app.route('/')
def main():
    """
    Setup the webpage.
    """

    initial_mu_xs = [-0.3529827892780304, -0.08488980680704117,
                     -0.13951079547405243, -0.6833804845809937,
                     0.44459542632102966]

    initial_mu_ys = [-0.0448102205991745, -0.4503704309463501,
                     0.5382919907569885, 0.2945760190486908,
                     -0.6999134421348572]

    initial_sigma_xs = [0.2883448898792267, 0.2929769456386566,
                        0.05536893382668495, 0.13594742119312286,
                        0.2586058974266052]

    initial_sigma_ys = [0.29675430059432983, 0.2704170048236847,
                        0.28341200947761536, 0.2986540198326111,
                        0.1882447898387909]

    initial_covs = [0.003889968153089285, -0.038692548871040344,
                    -0.0016177110373973846, -0.014885751530528069,
                    -0.03062933124601841]

    img_shape = 256
    bg_img_path = "../static/defaults/bg.jpg"
    bg_img = Image.open("flask_app/static/defaults/bg.jpg")
    bg_img_width = bg_img.width
    bg_img_height = bg_img.height

    for i, _ in enumerate(initial_mu_xs):
        initial_mu_xs[i] = str(initial_mu_xs[i])
        initial_mu_ys[i] = str(initial_mu_ys[i])
        initial_sigma_xs[i] = str(initial_sigma_xs[i])
        initial_sigma_ys[i] = str(initial_sigma_ys[i])
        initial_covs[i] = str(initial_covs[i])

    return render_template(
        'interactive.html.jinja',
        initial_mu_xs=initial_mu_xs,
        initial_mu_ys=initial_mu_ys,
        initial_sigma_xs=initial_sigma_xs,
        initial_sigma_ys=initial_sigma_ys,
        initial_covs=initial_covs,
        img_shape=img_shape,
        bg_img_height=str(bg_img_height),
        bg_img_width=str(bg_img_width),
        bg_img_path=bg_img_path)


@app.route('/api/run_landmarks', methods=['POST'])
def run_landmarks():
    """
    Sample gaussian landmark parameters.
    """

    request_content = request.get_json()
    bbx_region = np.array(request_content["bbx_region"])
    colors = np.array(request_content["gaussian_colors"])

    gauss_maps, mu_xs, mu_ys, sigma_xs, sigma_ys, covs = \
            model_runner.sample_l(bbx_region, colors)

    return jsonify({
        "gauss_maps": encode_img(gauss_maps),
        "mu_xs": mu_xs.flatten().tolist(),
        "mu_ys": mu_ys.flatten().tolist(),
        "sigma_xs": sigma_xs.flatten().tolist(),
        "sigma_ys": sigma_ys.flatten().tolist(),
        "covs": covs.flatten().tolist()})


@app.route('/api/run_gaussians', methods=['POST'])
def run_gaussians():
    """
    Generate new gaussian maps image from ellipses info.
    """

    request_content = request.get_json()
    mu_xs = np.array(request_content["gaussian_mu_xs"])
    mu_ys = np.array(request_content["gaussian_mu_ys"])
    sigma_xs = np.array(request_content["gaussian_sigma_xs"])
    sigma_ys = np.array(request_content["gaussian_sigma_ys"])
    covs = np.array(request_content["gaussian_covs"])
    colors = np.array(request_content["gaussian_colors"])

    gauss_maps = model_runner.gen_gaussians(
        mu_xs, mu_ys, sigma_xs, sigma_ys, covs, colors)

    return jsonify({"gauss_maps": encode_img(gauss_maps)})


@app.route('/api/run_models', methods=['POST'])
def run_models():
    """
    Run generative models to create mask and texture images.
    """

    request_content = request.get_json()
    bbx_region = np.array(request_content["bbx_region"])
    mu_xs = np.array(request_content["gaussian_mu_xs"])
    mu_ys = np.array(request_content["gaussian_mu_ys"])
    sigma_xs = np.array(request_content["gaussian_sigma_xs"])
    sigma_ys = np.array(request_content["gaussian_sigma_ys"])
    covs = np.array(request_content["gaussian_covs"])

    mask_img_numpy, texture_img_numpy = \
        model_runner.run_models(bbx_region, mu_xs, mu_ys,
                                sigma_xs, sigma_ys, covs)

    return jsonify({
        "mask_img": encode_img(mask_img_numpy),
        "texture_img": encode_img(texture_img_numpy)})


@app.route('/api/run_all', methods=['POST'])
def run_all():
    """
    Sample landmarks, and generate mask/texture images.
    """

    request_content = request.get_json()
    bbx_region = np.array(request_content["bbx_region"])
    colors = np.array(request_content["gaussian_colors"])

    return_vals = model_runner.run_all(bbx_region, colors)

    return jsonify({
        "gauss_maps": encode_img(return_vals[0]),
        "mu_xs": return_vals[1].flatten().tolist(),
        "mu_ys": return_vals[2].flatten().tolist(),
        "sigma_xs": return_vals[3].flatten().tolist(),
        "sigma_ys": return_vals[4].flatten().tolist(),
        "covs": return_vals[5].flatten().tolist(),
        "mask_img": encode_img(return_vals[6]),
        "texture_img": encode_img(return_vals[7])})


@app.route('/display/<filename>', methods=['GET'])
def display_image(filename):
    return redirect(url_for('static', filename=filename), code=301)


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(
        os.path.join(app.root_path, 'static'),
        'defaults/favicon.ico')


def encode_img(img):
    """
    Encode numpy image to base64.
    """

    if np.max(img) <= 1.:
        img *= 255.
    gauss_maps_img = Image.fromarray(img.astype(np.uint8))
    buffer = BytesIO()
    gauss_maps_img.save(buffer, format="PNG")
    return base64.b64encode(buffer.getvalue()).decode('utf-8')
