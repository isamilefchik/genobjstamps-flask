import cv2
from tensorpack import *
import numpy as np
import os
import pickle

# Note: region = np.array([x, y, width, height]) of selected bounding box region
def replacement_inference_cs_kas_draw_bbx(im, region, shape, shapez, shapez2, channel=3):
    """ Produce images read from a list of files. """

    im_orig = cv2.imread(im, cv2.IMREAD_COLOR)
    im_orig = im_orig[:, :, ::-1]
    template = np.zeros(im_orig.shape[:-1])
    region[2] = region[2] + region[0]
    region[3] = region[3] + region[1]
    template[region[1]:region[3], region[0]:region[2]] = 1
    box = find_bbx(template, shape)
    buffer = np.zeros_like(template)
    buffer[box[0]:box[2], box[1]:box[3]] = 1
    buffer = cv2.resize(buffer, (shape, shape))
    box_ = np.array([0, 0, 0, 0])
    xs = np.nonzero(np.sum(buffer, axis=0))[0]
    ys = np.nonzero(np.sum(buffer, axis=1))[0]
    box_[1] = xs.min()
    box_[3] = xs.max()
    box_[0] = ys.min()
    box_[2] = ys.max()

    im = im_orig[box[0]:box[2], box[1]:box[3], :]
    m = template[box[0]:box[2], box[1]:box[3]]
    im_orig = cv2.resize(im_orig, (shape, shape))
    im = cv2.resize(im, (shape, shape))
    maskj = cv2.resize(m, (shape, shape))
    maskj = np.expand_dims(maskj, axis=-1)
    box = np.array([0, 0, 0, 0])
    # Compute Bbx coordinates

    margin = 3
    bbx = np.zeros_like(maskj)
    xs = np.nonzero(np.sum(maskj, axis=0))[0]
    ys = np.nonzero(np.sum(maskj, axis=1))[0]
    box[1] = xs.min() - margin
    box[3] = xs.max() + margin
    box[0] = ys.min() - margin
    box[2] = ys.max() + margin

    if box[0] < 0: box[0] = 0
    if box[1] < 0: box[1] = 0
    if box[3] > shape: box[3] = shape - 1
    if box[2] > shape: box[2] = shape - 1

    if box[3] == box[1]:
        box[3] += 1
    if box[0] == box[2]:
        box[2] += 1

    bbx[box[0]:box[2], box[1]:box[3], :] = 1
    # box[2] = box[2] - box[0]
    # box[3] = box[3] - box[1]
    box = box * 1.
    box[0] = box[0] / shape
    box[2] = box[2] / shape
    box[1] = box[1] / shape
    box[3] = box[3] / shape
    box = np.reshape(box, [1, 1, 1, 4])

    z = np.random.normal(size=[1, 1, 1, shapez])
    z2 = np.random.normal(size=[1, 1, 1, shapez2])
    z3 = np.random.normal(size=[1, 1, 1, shapez])

    return (im[None, :, :, :], bbx[None, :, :, :], maskj[None, :, :, :], box, im_orig, box_, z, z2, z3)

def find_bbx(maskj, shape):
    resized_width = shape
    resized_height = shape
    maskj = np.expand_dims(maskj, axis=-1)

    box = np.array([0, 0, 0, 0])

    # Compute Bbx coordinates

    margin = 3
    xs = np.nonzero(np.sum(maskj, axis=0))[0]
    ys = np.nonzero(np.sum(maskj, axis=1))[0]
    box[1] = xs.min() - margin
    box[3] = xs.max() + margin
    box[0] = ys.min() - margin
    box[2] = ys.max() + margin

    if box[0] < 0: box[0] = 0
    if box[1] < 0: box[1] = 0

    h = box[2] - box[0]
    w = box[3] - box[1]
    if h < w:
        diff = w - h
        half = int(diff / 2)
        box[0] -= half
        if box[0] < 0:
            box[2] -= box[0]
            box[0] = 0
        else:
            box[2] += diff - half

        if box[2] > maskj.shape[0]:
            box[2] = maskj.shape[0]
    else:
        diff = h - w
        half = int(diff / 2)
        box[1] -= half
        if box[1] < 0:
            box[3] -= box[1]
            box[1] = 0
        else:
            box[3] += diff - half
        if box[3] > maskj.shape[1]:
            box[3] = maskj.shape[1]

    # if box[3] > resized_height: box[3] = resized_height - 1
    # if box[2] > resized_width: box[2] = resized_width - 1

    if box[3] == box[1]:
        box[3] += 1
    if box[0] == box[2]:
        box[2] += 1

    # bbx[box[0]:box[2], box[1]:box[3], :] = 1

    return box
